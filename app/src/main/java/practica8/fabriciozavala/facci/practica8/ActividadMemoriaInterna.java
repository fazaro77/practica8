package practica8.fabriciozavala.facci.practica8;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ActividadMemoriaInterna extends AppCompatActivity implements View.OnClickListener{

    EditText cajaCedula, cajaNombres, cajaApellidos, cajaDatos;;
    Button botonLeer, botonEscribir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_memoria_interna);

        cajaCedula = (EditText) findViewById(R.id.txtCedulaMI);
        cajaApellidos = (EditText) findViewById(R.id.txtApellidosMI);
        cajaNombres = (EditText) findViewById(R.id.txtNombresMI);
        cajaDatos = (EditText) findViewById(R.id.txtDatosMI);

        botonEscribir = (Button) findViewById(R.id.btnEscribiMI);
        botonLeer = (Button) findViewById(R.id.btnLeerMI);

        botonEscribir.setOnClickListener(this);
        botonLeer.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnEscribiMI:
                try {
                    OutputStreamWriter escritor = new OutputStreamWriter(openFileOutput("archivo.txt", Context.MODE_APPEND));
                    escritor.write(cajaCedula.getText().toString()+","+cajaApellidos.getText().toString()+","+cajaNombres.getText().toString());
                    escritor.close();
                }catch (Exception ex) {
                    Log.e("Archivo MI","Error en el archivo de escritura");
                }
                break;
            case R.id.btnLeerMI:
                try {
                    BufferedReader lector = new BufferedReader(new InputStreamReader(openFileInput("archivo.txt")));
                    String datos = lector.readLine();
                    String [] listaPersonas = datos.split(";");
                    for (int i = 0; i < listaPersonas.length; i++){
                        cajaDatos.append(listaPersonas[i].split(",")[0] + " " + listaPersonas[i].split(",")[1] + " " + listaPersonas[i].split(",")[2]);
                    }


                    lector.close();
                }catch (Exception ex){
                    Log.e("Archivo MI", "Error en la lectura del archivo"+ex.getMessage());
                }
                break;
        }
    }
}
